//
// Created by main on 21.09.2020.
//

#ifndef PRACTICE_1_LEAF_CPP
#define PRACTICE_1_LEAF_CPP

#include <string>
#include <Component.cpp>

class Leaf : public Component {
    int _number;
public:
    Leaf(int number = 0) : _number(number) {}

    std::string Show() const override {
        return std::to_string(_number);
    }
};
#endif //PRACTICE_1_LEAF_CPP
