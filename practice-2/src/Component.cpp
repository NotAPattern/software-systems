//
// Created by main on 21.09.2020.
//

#ifndef PRACTICE_1_COMPONENT_CPP
#define PRACTICE_1_COMPONENT_CPP

#include <string>

class Component {
protected:
    Component *parent_;

public:
    virtual ~Component() {}
    void SetParent(Component *parent) {
        this->parent_ = parent;
    }

    Component *GetParent() const {
        return this->parent_;
    }

    virtual void Add(Component *component) {}
    virtual void Remove(Component *component) {}

    virtual bool IsComposite() const {
        return false;
    }

    virtual std::string Show() const = 0;
};

#endif //PRACTICE_1_COMPONENT_CPP
