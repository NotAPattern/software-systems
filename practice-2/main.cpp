//
// Created by main on 21.09.2020.
//

#include <Component.cpp>
#include <Leaf.cpp>
#include <Composite.cpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <CompositeBuilder.cpp>

int main() {
    int n = 5;
    std::vector<int> an = {1, 2, 3, 4, 5};
    std::vector<int> bn = {5, 4, 3, 2};


    CompositeBuilder *builder1 = new CompositeBuilder;
    CompositeBuilder *builder2 = new CompositeBuilder;

    Component *tree1 = builder1 -> build(n, an);
    Component *tree2 = builder2 -> build(n, an, bn);

    std::cout << tree1 -> Show() << std::endl;
    std::cout << tree2 -> Show() << std::endl;

    delete tree1;
    delete tree2;

    return 0;
}
